# GNU Makefile for archives2git

PACKAGE_NAME	= archives2git
PACKAGE_TARNAME	= archives2git

# Get the version via git or from the VERSION file or from the project
# directory name.
VERSION	= $(shell test -x version.sh && ./version.sh $(PACKAGE_TARNAME) \
	  || echo "unknown_version")
# Allow either to be overwritten by setting DIST_VERSION on the command line.
PACKAGE_VERSION	= $(VERSION)
# Remove the _g<SHA1> part from the $VERSION
RPM_VERSION	= $(shell echo $(VERSION) | $(SED) -e 's/_g[0-9a-z]\+//')

prefix	= $(HOME)/.local
bindir  = $(prefix)/bin
datarootdir = $(prefix)/share
docdir  = $(datarootdir)/doc/$(PACKAGE_TARNAME)
mandir  = $(datarootdir)/man
# DESTDIR =  # distributors set this on the command line

CP	?= cp
MKDIR	= mkdir
INSTALL	= install
SED	?= sed
TAR	?= tar
TAR_FLAGS	= --owner root --group root --mode a+rX,o-w --mtime .
HELPM2POD	= helpm2pod
POD2MAN	= pod2man
POD2MAN_FLAGS = --utf8 -c "" -r "$(PACKAGE_NAME) $(VERSION)"
POD2TEXT	= pod2text
POD2HTML	= pod2html
ASCIIDOC	= asciidoc
ASCIIDOC_FLAGS	= -apackagename="$(PACKAGE_NAME)" -aversion="$(VERSION)"
MD5SUM	?= md5sum
SHA512SUM	?= sha512sum

SCRIPTS	= archives2git dsc2git
MANDOC	= $(SCRIPTS:%=%.1)
TEXTDOC	= $(SCRIPTS:%=%.1.txt)
HTMLDOC	= $(SCRIPTS:%=%.1.html) README.html NEWS.html
ALLDOC	= $(MANDOC) $(TEXTDOC) $(HTMLDOC)
RELEASEDOC	= $(MANDOC) $(HTMLDOC)

TARNAME	= $(PACKAGE_TARNAME)-$(RPM_VERSION)

.PHONY: .help all clean build doc doc-txt doc-man doc-html dist \
	install install-doc

all: build doc

.help:
	@echo "Useful targets:"
	@echo "	doc doc-man doc-html doc-txt clean distclean dist"
	@echo "	install install-doc"

clean:
	$(RM) *.1.helpm *.pod pod2htmd.tmp pod2htmi.tmp *~ .*~
	$(RM) $(PACKAGE_TARNAME)-*.tar.gz*
distclean: clean
	$(RM) ChangeLog $(ALLDOC)

install: build install-doc
	$(MKDIR) -p $(DESTDIR)$(bindir)
	$(INSTALL) -m 755 $(SCRIPTS) $(DESTDIR)$(bindir)/
install-doc:
	$(MKDIR) -p $(DESTDIR)$(mandir)/man1
	$(INSTALL) -m 644 $(MANDOC) $(DESTDIR)$(mandir)/man1/

build:
doc: $(ALLDOC)
doc-txt: $(TEXTDOC)
doc-man: $(MANDOC)
doc-html: $(HTMLDOC)

$(TARNAME).tar: ChangeLog
	$(MKDIR) -p $(TARNAME)
	echo $(VERSION) > $(TARNAME)/VERSION
	$(CP) -p ChangeLog $(TARNAME)
	git archive --format=tar --prefix=$(TARNAME)/ HEAD > $(TARNAME).tar
	$(TAR) $(TAR_FLAGS) -rf $(TARNAME).tar $(TARNAME)
	$(RM) -r $(TARNAME)
dist: $(TARNAME).tar.gz
$(TARNAME).tar.gz: $(TARNAME).tar $(RELEASEDOC)
	$(MKDIR) $(TARNAME)
	$(CP) -p -P $(RELEASEDOC) $(TARNAME)
	$(TAR) $(TAR_FLAGS) -rf $(TARNAME).tar $(TARNAME)
	$(RM) -r $(TARNAME)
	gzip -f -9 $(TARNAME).tar
	$(MD5SUM) $(TARNAME).tar.gz > $(TARNAME).tar.gz.md5
	$(SHA512SUM) $(TARNAME).tar.gz > $(TARNAME).tar.gz.sha512
ChangeLog:
	( echo "# $@ for $(PACKAGE_NAME) - automatically generated from the VCS's history"; \
	  echo; \
	  ./gitchangelog.sh --tags --tag-pattern 'version\/[^\n]*' \
	    -- - --date-order --first-parent ) \
	| $(SED) 's/^\[version/\[version/' \
	> $@

README.html: README asciidoc.conf
	$(ASCIIDOC) $(ASCIIDOC_FLAGS) -b xhtml11 -d article -a readme $<
NEWS.html: NEWS asciidoc.conf
	$(ASCIIDOC) $(ASCIIDOC_FLAGS) -b xhtml11 -d article $<

%.1.helpm: %
	./$< --helpm >$@
%.pod: %.1.helpm
	$(HELPM2POD) $< >$@

%.1: %.pod
	$(POD2MAN) $(POD2MAN_FLAGS) --section 1 $< >$@
%.1.txt: %.pod
	$(POD2TEXT) --utf8 $< >$@
%.1.html: %.pod
	$(POD2HTML) --noindex --title "$(shell printf "%s(1)" "$*" | LC_ALL=C tr a-z A-Z)" \
	  $< >$@
